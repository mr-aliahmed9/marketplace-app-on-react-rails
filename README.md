# Simple Marketplace Project - React JS as a seprate App inside a Rails API only App
This application has a separate implementation of its frontend and backend. React, using *npm*, is used to store/fetch data from the backend and Rails, as a API only app, 
talks in JSON. React app codebase is inside the `client` directory and is built on the top of [Redux](https://redux.js.org/introduction/getting-started) to manage the app wide state.

### Features
- Full JWT Authention (Both Frontend and Backend)
- Simple CRUD Operations
- Searching Functionality

## Pre-requisites
- Rails 5.0.5
- Ruby 2.5.3
- Npm
- Postgres Latest

## Installation
 - Run `bundle`
 - Run `cp config/database.yml.exmample config/database.yml`
 - Provide Database credentials
 - Run `cd client && npm install`
 - Run 'cd ..'
 - Run `foreman start`
 - Go to `localhost:3000` and play around.